# -*- coding: utf-8 -*-
import cv2
import numpy as np
from matplotlib import pyplot as plt
from skimage.filters import roberts, sobel

#Exercício 1 - Imagem Menina - Equalização
imagemMenina = cv2.imread('menina.jpg')
imgMeninaCinza = cv2.cvtColor(imagemMenina, cv2.COLOR_BGR2GRAY) # converte para cinza
cv2.imshow("Imagem Menina - Original",imgMeninaCinza)
plt.hist(imgMeninaCinza.ravel(),256,[0,256])
plt.title('Histograma para a imagem original em escala de cinza')
plt.show()
#Função da cv2 converte a imagem de um espaço de cor para outro, no caso converte de BGR para YUV
imagem_para_yuv = cv2.cvtColor(imagemMenina, cv2.COLOR_BGR2YUV)
#Função de equalização da OpenCV para equalização de histograma no canal YUV
imagem_para_yuv[:,:,0] = cv2.equalizeHist(imagem_para_yuv[:,:,0])
#Função da cv2 converte a imagem de um espaço de cor para outro, no caso converte de YUV de volta para BGR
histograma_equalizado = cv2.cvtColor(imagem_para_yuv, cv2.COLOR_YUV2BGR)
cv2.imwrite('meinaEqualizada.jpg',histograma_equalizado)
imagemMeninaEqualizada = cv2.imread('meinaEqualizada.jpg')
cv2.imshow("Imagem Menina - Equalizada",imagemMeninaEqualizada)
plt.hist(imagemMeninaEqualizada.ravel(),256,[0,256])
plt.title('Histograma para a imagem equalizada')
plt.show()
cv2.waitKey(0)
cv2.destroyAllWindows()

#Exercício 2 - Imagem FiltroMediana - Aplicação de filtro de média e de mediana.
imagemMediana = cv2.imread('filtromediana.png')
imgMedianaCinza = cv2.cvtColor(imagemMediana, cv2.COLOR_BGR2GRAY) # converte para cinza
#Aplicação do filtro de média na imagem
imagemFiltroMedia = cv2.blur(imgMedianaCinza,(5,5))
#Concatenação da imagem original com a filtrada
finalImage = np.concatenate((imgMedianaCinza, imagemFiltroMedia), axis=0)
cv2.imshow("Imagem Original acima vs Imegem com filtro de media abaixo", finalImage)
#Aplicação do filtro de mediana na imagem
imagemFiltroMediana = cv2.medianBlur(imgMedianaCinza,5)
#Concatenação da imagem original com a filtrada
finalImage2 = np.concatenate((imgMedianaCinza, imagemFiltroMediana), axis=0)
cv2.imshow("Imagem Original acima vs Imegem com filtro de mediana abaixo", finalImage2)
cv2.waitKey(0)
cv2.destroyAllWindows()

#Exercício 3 - Imagem ImpressaoDigital - Filtros de prewitt, sobel, roberts e canny
imagemImpressao = cv2.imread('impressaodigital.png')
imgImpressaoCinza = cv2.cvtColor(imagemImpressao, cv2.COLOR_BGR2GRAY) # converte para cinza
#Aplicando filtro de prewitt
imgImpressaoGaussian = cv2.GaussianBlur(imgImpressaoCinza,(3,3),0)
kernelx = np.array([[1,1,1],[0,0,0],[-1,-1,-1]])
kernely = np.array([[-1,0,1],[-1,0,1],[-1,0,1]])
imgImpressaoPrewittx = cv2.filter2D(imgImpressaoGaussian, -1, kernelx)
imgImpressaoPrewitty = cv2.filter2D(imgImpressaoGaussian, -1, kernely)
imgImpressaoPrewitt = imgImpressaoPrewittx + imgImpressaoPrewitty
#Aplicando filtro de canny
imgImpressaoCanny = cv2.Canny(imgImpressaoCinza,100,200)
#Aplicando método de Sobel e Roberts
bordaRoberts = roberts(imgImpressaoCinza)
bordaSobel = sobel(imgImpressaoCinza)
fig, ax = plt.subplots(ncols=2, sharex=True, sharey=True, figsize=(8, 4))
ax[0].imshow(bordaRoberts, cmap=plt.cm.gray)
ax[0].set_title('Imagem Impressao Digital - Roberts')
ax[1].imshow(bordaSobel, cmap=plt.cm.gray)
ax[1].set_title('Imagem Impressao Digital - Sobel')
for a in ax:
    a.axis('off')
plt.tight_layout()
plt.show()
cv2.imshow("Imagem Impressao Digital Original", imgImpressaoCinza)
cv2.imshow("Imagem Impressao Digital - Prewitt completo", imgImpressaoPrewitt)
cv2.imshow("Imagem Impressao Digital - Canny", imgImpressaoCanny)
cv2.waitKey(0)
cv2.destroyAllWindows()

#Exercício 4 - Imagem Exame - Detectar bordas de globos
imagemExame = cv2.imread('MicroHematuria.jpg')
imgExameCinza = cv2.cvtColor(imagemExame, cv2.COLOR_BGR2GRAY) # converte para cinza
#Aplicando filtro de canny para detectar bordas
imgExameCanny = cv2.Canny(imgExameCinza,100,200)
cv2.imshow("Imagem Exame Original", imgExameCinza)
cv2.imshow("Imagem Exame - Detectar globos", imgExameCanny)
cv2.waitKey(0)
cv2.destroyAllWindows()